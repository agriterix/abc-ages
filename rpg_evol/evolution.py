#!/usr/bin/python3
# -*- coding: utf-8 -*-
import yaml
import geopandas as gpd
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import json


class Evolution:
    '''
    Tool to observe farming types evolution in Auvergne-Rhône-Alpes region.
    This class generates json data files and plot them.
    '''

    def __init__(self):
        '''
        Simple init indexing resource files needed. 
        '''
        file = open("resources/index.yml")
        self.index = yaml.load(file, Loader=yaml.FullLoader)
        file.close()

    def hist_surface_culture(self, year):
        '''
        Generates file of total surface by culture type.
        :param year year in [2015, 2016, 2018, 2019]
        '''
        rpg = gpd.read_file(self.index["rpg_aura_" + str(year)]["file"])
        rpg = rpg.drop(["geometry"], axis=1)

        rpg.CODE_GROUP = pd.to_numeric(rpg.CODE_GROUP)
        codes = np.sort(pd.unique(rpg.CODE_GROUP))

        hist = {}
        for i in codes:
            i = int(i)
            hist[i] = sum(rpg[rpg.CODE_GROUP.eq(i)]["SURF_PARC"].values)
            hist[i] = float(hist[i])
        if not os.path.exists("out/hist"):
            os.makedirs("out/hist")
        with open("out/hist/" + str(year) + ".json", "w") as fp:
            json.dump(hist, fp)

    def draw_evolution(self):
        '''
        Draws in `out/fig/` evolution plots by categories.
        '''
        df = pd.DataFrame(columns=[str(i) for i in range(1, 29)])
        for i in [2015, 2016, 2018, 2019]:
            with open("out/hist/" + str(i) + ".json") as file:
                df.loc[i] = json.load(file)
        df["autre"] = df[["5", "6", "7", "8", "9", "10", "14", "15", "16", "24", "26",
                          "11", "12", "13", "28", "20", "22", "23", "27", "21", "25"]].sum(axis=1)
        df["elevage"] = df[["17", "18", "19"]].sum(axis=1)
        df["cereales"] = df[["1", "2", "3", "4"]].sum(axis=1)
        df["total"] = df.sum(axis=1)
        if not os.path.exists("out/fig"):
            os.makedirs("out/fig")
        for i in ["autre", "elevage", "cereales", "total"]:
            plt.scatter([2015, 2016, 2018, 2019], df[i])
            plt.plot([2015, 2016, 2018, 2019], df[i])
            plt.xticks([2015, 2016, 2018, 2019])
            plt.xlabel("années")
            plt.ylabel("surface totale")
            plt.title("Evolution " + i + " RPG (Aura)")
            plt.ticklabel_format(style='plain')
            plt.savefig("out/fig/evol_" + i + ".png")
            plt.clf()


if __name__ == "__main__":
    evolution = Evolution()
    if len(sys.argv) > 1:
        evolution.hist_surface_culture(sys.argv[1])
    else:
        evolution.draw_evolution()
