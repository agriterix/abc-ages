#!/usr/bin/python3
# -*- coding: utf-8 -*-
sys.path.append("abc-ages/")
import population_model
from ast import literal_eval
import h5py
import os
import geopandas as gpd
import numpy as np
import pandas as pd
import sys


class RPGbyPRAModel:
    def __init__(self, *args):
        if args:
            self.farmers = pd.read_csv(args[0])
            self.farmers["culture"] = self.farmers["culture"].apply(
                literal_eval)
        else:
            if not os.path.exists("out/rpg_pra/rpg_pra.shp"):
                rpg_pra = self.create_rpg_pra()
            else:
                rpg_pra = gpd.read_file("out/rpg_pra/rpg_pra.shp")
            self.farmers = pd.DataFrame(
                columns=["id_expl", "age", "cultures", "pra"])
            self.farmers["id_expl"] = rpg_pra.id_expl.drop_duplicates(
            ).sort_values()
            self.farmers["cultures"] = [[
                (int(j[0]), float(j[1])) for j in rpg_pra[rpg_pra.id_expl.eq(
                    i)][["code_group", "surface_gr"]].values
            ] for i in self.farmers.id_expl]
            self.farmers["pra"] = [
                rpg_pra[rpg_pra.id_expl.eq(i)]["name_pra"].values[0]
                for i in self.farmers.id_expl
            ]

    def export(self, filename):
        if not os.path.exists("out/farmers"):
            os.makedirs("out/farmers")
        self.farmers.to_csv("out/farmers/" + filename, sep=";")

    def gen_age(self, ages_hist):
        ages = np.arange(18, 101)
        probas = [i / sum(ages_hist) for i in ages_hist]
        self.farmers["age"] = (np.random.choice(
            range(len(ages)), size=len(self.farmers), p=probas) + 18)

    def create_rpg_pra(self):
        # PRA
        pra = gpd.read_file("resources/pra/pra.shp")
        pra.geometry = pra.geometry.to_crs(2154)
        pra = pra.drop([716, 717, 718, 719, 720])
        # REGIONS
        reg = gpd.read_file("resources/regions/regions-20161121.shp")
        reg.geometry = reg.geometry.to_crs(2154)
        reg = reg.drop([0, 1, 2, 3, 4])
        # RPG
        rpg = gpd.read_file(
            "resources/RPG_id/l_ilot_anonyme_groupe_dominant_s_r84_2016.shp")
        # AURA
        aura = reg[reg.code_insee.eq("84")]
        aura.loc[15].geometry
        pra_aura = pra[pra.geometry.centroid.within(aura.loc[15].geometry)]
        # RAM cleaning
        del reg, aura, pra
        # Join rpg/pra
        rpg_pra = gpd.tools.sjoin(rpg, pra_aura, op="within", how="left")
        # RAM cleaning
        del rpg, pra_aura
        # Export
        os.makedirs("out/rpg_pra")
        rpg_pra.to_file("out/rpg_pra/rpg_pra.shp", driver="ESRI Shapefile")
        return rpg_pra

    def transition(self, scenario, years):
        """
        Simulates a retirement/installation tranisition.
        :param scenario: a list of rpg culture codes (1..28) who will be
        chosen as new culture when there is an installation.
        :param years: number of years elapsed.
        """
        hist_start = np.histogram(self.farmers["age"], bins=range(18, 101))[0]
        model = population_model.PopulationModel(hist_start,
                                                 "out/abc_50000_param.csv",
                                                 "out/abc_50000_weights.csv",
                                                 nbsamples=1)

        for i in range(years):
            hist_end, installs, retires = model.step_year()

        installation = int(sum(installs[0]))
        retirement = int(sum(retires[0]))

        retirement_treshold = 60

        self.farmers["age"] = self.farmers["age"] + years
        sample = self.farmers[
            self.farmers["age"] < retirement_treshold].sample(retirement)

        new_sample = sample.sample(installation)
        new_sample["cultures"] = new_sample["cultures"].apply(
            lambda x: [(np.random.choice(scenario), i[1]) for i in x])
        self.farmers = self.farmers.drop(list(sample.index.values))
        self.farmers = pd.concat([new_sample, self.farmers], axis=0)


if __name__ == "__main__":
    model = RPGbyPRAModel()
    model.gen_age([
        5, 47, 194, 529, 1056, 1760, 2477, 3184, 4013, 4617, 5210, 5495, 5981,
        6354, 6949, 7646, 8706, 9973, 10766, 11644, 12479, 13180, 13736, 14781,
        15885, 16682, 17803, 17706, 17987, 18386, 18158, 17949, 17533, 18011,
        17308, 17259, 17538, 18032, 18606, 17364, 16668, 15279, 13731, 7604,
        5294, 3282, 2669, 2286, 1614, 1236, 1007, 1074, 978, 829, 789, 655,
        697, 591, 496, 471, 403, 348, 323, 269, 248, 240, 220, 171, 178, 201,
        248, 130, 58, 63, 43, 42, 55, 42, 30, 30, 9, 10, 15
    ])
    model.export("farmers")
    model.transition([1, 2, 3, 4], 10)
    print(model.farmers.head())
