# RPG Evolution

Repository hosting data analysis and projection regarding culture types and age.

## Data structure


<!--
| id_expl | age |  1  |  2  | ... | 28  | pra |
|:----:|:-------:|:---:|:---:|:---:|:---:|-----|-----|
| ...  |   ...   | ... | ... | ... | ... | ... |     |

pb hdf5/pandas : 

- https://github.com/pandas-dev/pandas/issues/20440
- https://medium.com/@jerilkuriakose/using-hdf5-with-python-6c5242d08773
- https://dzone.com/articles/quick-hdf5-pandas -->